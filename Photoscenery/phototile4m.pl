#!/usr/bin/perl

use POSIX;
use Carp;
use LWP::Simple;
use Math::Trig;
use Math::Trig qw(great_circle_distance great_circle_destination great_circle_bearing deg2rad rad2deg);
use Getopt::Long;
use File::Slurp;
use Geo::Coordinates::RDNAP qw/to_rd/;
use Image::Size;

# change these options to match your system
$imagemagick = "C:/Program Files (x86)/ImageMagick-6.7.3-Q16";
$nvcompress = "C:/Program Files (x86)/NVIDIA Corporation/NVIDIA Texture Tools 2/bin/nvcompress";

# default settings
$source = "DKLN2006";   # could also be set to "NAIP" for the USA. Please read the associated licenses before using!!
$nodownload = 0;		# set to true to disable downloading (eg. when the images are already present on your system)

# defaults for tile 3023745
# $tile=3023745;
# $ymin=446346;
# $ymax=460014;
# $xmin=76908;
# $xmax=94244;

# defaults for tile 3023737		
#$tile=3023737;
#$ymin=432441;
#$ymax=446107;
#$xmin=76690;
#$xmax=94075;

$result = GetOptions ("no-download"    => \$nodownload,
                      "lat=f"          => \$lat,
                      "lon=f"          => \$lon,
                      "tile=f"         => \$tile,
                      "src=s"          => \$source,
                      "v|verbose"      => \$verbose );

if ($tile != nil) {
	coordinatesFromTileIndex();
} else {
	tileIndexFromCoordinate();
	coordinatesFromTileIndex();
};
				
sub tileWidth {
	my ($lat)=@_;
	@tiletable = ([0,0.125],[22,0.25],[62,0.5],[76,1],[83,2],[86,4],[88,8],[89,360],[90,360]);
	for($i=0 ; $i<=scalar(@tiletable) ; $i++) {
		if (abs($lat)>=@tiletable[$i]->[0] and abs($lat)<@tiletable[$i+1]->[0]) {
			return (@tiletable[$i]->[1]);
		};
	};
};

sub coordinatesFromTileIndex {
	$base_x    = ($tile>>14) - 180;
	$base_y    = (($tile-(($base_x+180)<<14)) >>6) - 90;
	$y         =  ($tile-((($base_x+180)<<14)+ (($base_y+90) << 6))) >> 3;
	$x         =  $tile-((((($base_x+180)<<14)+ (($base_y+90) << 6))) + ($y << 3));
	$tilewidth = tileWidth($base_y);
	$ymin = $base_y + 0.125 * $y;
	$ymax = $base_y + 0.125 * ($y+1);
	$xmin = $base_x + $x * $tilewidth;
	$xmax = $base_x + ($x+1) * $tilewidth;
	$ycenter = 0.5 * ($base_y+0.125*$y + $base_y + 0.125*($y+1));
	$xcenter = 0.5 * ($base_x + $x * $tilewidth + $base_x + ($x+1) * $tilewidth);
	
	print "xmin: ".$xmin."\n" if $verbose;
	print "xmax: ".$xmax."\n" if $verbose;
	print "ymin: ".$ymin."\n" if $verbose;
	print "ymax: ".$ymax."\n" if $verbose;
	
	# Calculate RD/Amersfoort coordinates
	($bla, $yminRD, $h) = to_rd( $ymin, $xmax, 0 );
	($xpoint, $ymaxRD, $h) = to_rd( $ymax, $xmin, 0 );
	($xminRD, $bla, $h) = to_rd( $ymin, $xmin, 0 );
	($xmaxRD, $ypoint, $h) = to_rd( $ymax, $xmax, 0 );
	
	# Calculate rotation in degrees
	# BIG NOTE: this works for locations WEST of Amersfoort. 
	# 			EAST of Amersfoort the rotation becomes negative.
	$radians = tan(($ymaxRD-$ypoint)/($xmaxRD-$xpoint));
	$degrees = rad2deg($radians);
	
	print "xminRD: ".$xminRD."\n" if $verbose;
	print "xmaxRD: ".$xmaxRD."\n" if $verbose;
	print "yminRD: ".$yminRD."\n" if $verbose;
	print "ymaxRD: ".$ymaxRD."\n" if $verbose;
	print "Rotation (degrees): ".$degrees."\n" if $verbose;
};

sub tileIndexFromCoordinate {
	$base_y    = floor($lat);
	$y         = int(($lat-$base_y)*8);
	$tilewidth = tileWidth($lat);
	$base_x    = floor(floor( $lon / $tilewidth )* $tilewidth );
	if ($base_x < -180) {
		$base_x=-180;
		};
	$x         = int(floor(($lon-$base_x)/$tilewidth));
	$tile = int(((int(floor($lon))+180)<<14) + ((int(floor($lat))+ 90) << 6) + ($y << 3) + $x);
};


if ($source == "DKLN2006") {
	$tiles = 8; #4m/pixel
	$tiles = 4; #8m/pixel
};

# calculate photo-tile size
$xtile=($xmaxRD-$xminRD)/$tiles;
$ytile=($ymaxRD-$yminRD)/$tiles;

if (!$nodownload) {
	for($i=0; $i<$tiles ;$i++) {
		for($j=0; $j<$tiles; $j++){
			$left = $xminRD+($xtile*$i);
			$right = $xminRD+($xtile*($i+1));
			$bottom = $yminRD+($ytile*$j);
			$top = $yminRD+($ytile*($j+1));

			if ($source == "DKLN2006") {
				# Download DKLN2006 image
				$url = "http://gdsc.nlr.nl/wms/dkln2006?request=GetMap&service=WMS&version=1.1.1&layers=dkln2006-1m&SRS=EPSG:28992";
			} elsif ($source == "NAIP") {
				# Download USGS_EDC_Ortho_NAIP image
				$url = "http://isse.cr.usgs.gov/ArcGIS/services/Combined/SDDS_Imagery/MapServer/WMSServer?request=getmap&version=1.1.1&layers=0&styles=default&SRS=EPSG:4326";
			};
			
			# Calculate image width and height in pixels (4 rows/columns, 2048x2048 end result)
			$imagesize = floor(4096*(sin($radians)+cos($radians))/$tiles);
			
			$url = $url."&BBOX=".$left.",".$bottom.",".$right.",".$top."&FORMAT=image/png&width=".$imagesize."&height=".$imagesize;
			my $file = $tile.".".$i.".".$j.".png";
			mkdir($tile);
			print "Downloading photo from: ".$url."\n" if $verbose;
			getstore($url, $tile."/".$file);
			
			# Flip image upside-down
			system("\"".$imagemagick."/convert\" -flop ".$tile."/".$file." ".$tile."/".$file);
		}
	}

	for($i=0; $i<$tiles; $i++) {
		for($j=0; $j<$tiles; $j++){
			# Generate vertical bands
			system("\"".$imagemagick."/convert\" ".$tile."/".$tile.".".$i.".3.png ".$tile."/".$tile.".".$i.".2.png ".$tile."/".$tile.".".$i.".1.png ".$tile."/".$tile.".".$i.".0.png -append ".$tile."/".$tile.".".$i.".png");
			#Flip image upside-down
			system("\"".$imagemagick."/convert\" -flop ".$tile."/".$tile.".".$i.".png ".$tile."/".$tile.".".$i.".png");
		}
	}
}

# Skip this when nodownload is set, only convert png to dds (after manually cropping)
if (!$nodownload) {
	# Create final texture, by adding vertical bands
	system("\"".$imagemagick."/convert\" ".$tile."/".$tile.".0.png ".$tile."/".$tile.".1.png ".$tile."/".$tile.".2.png ".$tile."/".$tile.".3.png +append ".$tile."/".$tile.".png");

	# Add some contrast
	system("\"".$imagemagick."/convert\" ".$tile."/".$tile.".png -level 15% ".$tile."/".$tile.".png");

	# Rotate image, to line it up with the FG coordinate system
	system("\"".$imagemagick."/convert\" -rotate -".$degrees." ".$tile."/".$tile.".png ".$tile."/".$tile.".png");

	# Calculate shave size, to cut away the white triangles that we got due to the rotation
	# Retrieve actual image size
	($globe_x, $globe_y) = imgsize($tile."/".$tile.".png");
	print "Detected image size: ".$globe_x." pixels width (and high)\n";
	$shave = floor(($globe_x-4096)/2);
	print "Shaving ".$shave." pixels from all four sides.\n" if $verbose;
	system("\"".$imagemagick."/convert\" -shave ".$shave."x".$shave." ".$tile."/".$tile.".png ".$tile."/".$tile.".png");
}

# Crop image if it is not a power of two
system("\"".$imagemagick."/convert\" -crop 4096x4096+0+0 ".$tile."/".$tile.".png ".$tile."/".$tile.".png");

# Convert png to dds
print "Converting to .dds\n" if $verbose;
system("\"".$nvcompress."\"", "-clamp", "-bc1", $tile."/".$tile.".png");
print "\nDone!\nPlease move ".$tile."/".$tile.".dds to its corresponding scenery directory.\n" if $verbose;

exit 0;