Bob is a builder, he'll happily try to make a building from any OpenStreetMap
way. He's not yet very bright so doesn't understand things like multipolygons
though you can create a set of models which can be easily combined by using
the same reference point for all of them.

The script should provide you with an accuratly shaped and positioned model
which can form the basis of further development.

Original very ugly script by Jon Stockill.
